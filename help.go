package main

import (
    "fmt"
)

var cmdHelp = &Command{
    Name:        "help",
    Description: "List available commands or shows inline help on command",
}

var usageTemplate = `Gobs is a build system and package manager for go language

Usage:
    gobs command [arguments]

The commands are:
{{range .}}
    {{printf "%-8s - %s" .Name .Description}}{{end}}

Use "gobs help [command]" for more information.

`

func printUsage() {
    renderTemplate(usageTemplate, commands)
}

func help(args []string) {
    if len(args) != 1 {
        printUsage()
        return
    }

    cmd := getCommand(args[0])
    if cmd == nil {
        showUnknownCommandAndExit(args[0])
    }

    if cmd.HelpText == "" {
        fmt.Fprintf(OUT, "No help for command \"%s\"\n", args[0])
        exitProcess(-1)
    }

    fmt.Fprintf(OUT, cmd.HelpText)
}

func init() {
    cmdHelp.Executor = help
}
