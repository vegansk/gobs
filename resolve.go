package main

import (
    "bitbucket.org/vegansk/vcstools"
    "flag"
    "fmt"
    "os"
    "os/exec"
)

var cmdResolve = &Command{
    Name:        "resolve",
    Description: "Resolve package dependencies",
    HelpText:    resolveHelp,
}

const resolveHelp = `
"resolve" command
=================

This comand is used to resolve project dependenices based on
it's configuration file named "goproject.json"

Usage:

    resolve [flags] [packages...]

, where packages is optional package import name list. If not
specified, then all dependenices are resolved. The flags are:

    -u
        Update package(s)

`

// Flags
var (
    flUpdate bool
)

func getUpdatingDeps(args []string, proj *Project) (deps Dependencies) {
    deps = make(Dependencies)
    switch {
    case len(args) == 0:
        for k, v := range proj.Deps {
            deps[k] = v
        }
    default:
        for _, v := range args {
            tag, ok := proj.Deps[v]
            if !ok {
                showErrorAndExit(fmt.Errorf("Unknown dependency %s", v))
            }
            deps[v] = tag
        }
    }
    return
}

func depError(dep string, err interface{}) {
    if err != nil {
        showErrorAndExit(fmt.Errorf("Error resolving dependency %s: %v", dep, err))
    }
}

func resolveSingleDependency(dep, tag string, proj *Project) {
    fmt.Fprintf(OUT, "Resolving dependency: %s [%s]\n", dep, tag)
    depRoot := proj.GetDependencyDir(dep)
    if !isPathExists(depRoot) {
        args := []string{"get"}
        args = append(args, dep)
        cmd := exec.Command("go", args...)
        cmd.Stdin, cmd.Stdout, cmd.Stderr = os.Stdin, os.Stdout, os.Stderr
        cmd.Env = proj.CreateEnv().slice()
        err := cmd.Run()
        if err != nil {
            showErrorAndExit(err)
        }
    }
    vcs, err := vcstools.Open(depRoot)
    depError(dep, err)
    if flUpdate {
        err = vcs.Update()
        depError(dep, err)
    }

    err = vcs.CheckoutTag(tag)
    depError(dep, err)
}

func doInstall(proj *Project) {
    for _, v := range proj.Install {
        fmt.Fprintf(OUT, "Installing %s\n", v)
        cmd := exec.Command("go", "install", v)
        cmd.Stdin, cmd.Stdout, cmd.Stderr = os.Stdin, os.Stdout, os.Stderr
        cmd.Env = proj.CreateEnv().slice()
        err := cmd.Run()
        if err != nil {
            showErrorAndExit(err)
        }
    }
}

func resolve(args []string) {
    proj := mustLoadProject()
    deps := getUpdatingDeps(args, proj)
    for dep, tag := range deps {
        resolveSingleDependency(dep, tag, proj)
    }
    doInstall(proj)
}

func init() {
    cmdResolve.Executor = resolve

    f := flag.NewFlagSet("resolve", flag.ContinueOnError)
    f.Usage = getUsageFunc(cmdResolve)
    f.BoolVar(&flUpdate, "u", false, "")

    cmdResolve.Flags = f
}
