package main

import (
    "flag"
    "fmt"
    "os"
)

// Command descriptor
type Command struct {
    // Command name
    Name string

    // Command executor
    Executor func(args []string)

    // Command flags
    Flags *flag.FlagSet

    // Short description for help command
    Description string

    // Help text for command
    HelpText string
}

var commands = []*Command{
    cmdHelp,
    cmdCreateProject,
    cmdInitProject,
    cmdTags,
    cmdEdit,
    cmdShell,
    cmdResolve,
    cmdBuild,
    cmdClean,
}

func getCommand(name string) *Command {
    for _, cmd := range commands {
        if cmd.Name == name {
            return cmd
        }
    }

    return nil
}

func getUsageFunc(cmd *Command) func() {
    return func() {
        cmdHelp.Executor([]string{cmd.Name})
    }
}

func showHelpAndExit() {
    cmdHelp.Executor(nil)
    exitProcess(-1)
}

func showUnknownCommandAndExit(cmd string) {
    fmt.Printf("Unknown command: %s\n\n", cmd)
    showHelpAndExit()
}

func main() {
    defer (func() {
        deinitUtils()
        if err := recover(); err != nil {
            switch err := err.(type) {
            case error:
                fmt.Println(err.Error())
            default:
                fmt.Println(err)
            }
        }
    })()

    var args []string
    args = append(args, os.Args[1:]...)

    if len(args) == 0 {
        showHelpAndExit()
    }

    if cmd := getCommand(args[0]); cmd != nil {
        args = args[1:]
        if cmd.Flags != nil {
            if err := cmd.Flags.Parse(args); err != nil {
                exitProcess(-1)
            }
            args = cmd.Flags.Args()
        }
        if cmd.Executor != nil {
            cmd.Executor(args)
        } else {
            cmdHelp.Executor([]string{cmd.Name})
        }
        exitProcess(0)
    }

    showUnknownCommandAndExit(args[0])
}
